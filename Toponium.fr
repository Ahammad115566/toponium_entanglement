(* ************************** *)
(* *****  Information   ***** *)
(* ************************** *)
M$ModelName = "Toponium";

M$Information = {
  Authors      -> {"Ahmed Hammad"}, 
  Version      -> "",
  Date         -> "",
  Institutions -> {""}.
  Emails       -> {"ahhammad@cern.ch"},
  URLs         -> ""
};

FeynmanGauge = False;

(* *****  Change  log   ***** *)
(* ************************** *)

(* v0.1: Initial version                      *)


(* ************************** *)
(* *** Interaction orders *** *)
(* ***  (as used by mg5)  *** *)
(* ************************** *)
M$InteractionOrderHierarchy = {
  {QS0, 1}
  
};


(* ************************** *)
(* **** Particle classes **** *)
(* ************************** *)
M$ClassesDescription = {

 S[22] =={
    ClassName       -> SA0,
    SelfConjugate   -> True,
    Mass            -> {MA0, 344},
    Width           -> {WA0, Internal},
    PropagatorLabel -> "A0",
    PropagatorType  -> D,
    PropagatorArrow -> None,
    PDG             -> 6000047,
    ParticleName    -> "SA0",
    FullName        -> "SA0"
  }
}

(* ************************** *)
(* *****     Gauge      ***** *)
(* *****   Parameters   ***** *)
(* *****   (FeynArts)   ***** *)
(* ************************** *)


(* ************************** *)
(* *****   Parameters   ***** *)
(* ************************** *)
M$Parameters = {
  (* *****   S0   ***** *)
  s0scalar == {
	ParameterType -> External,
	ParameterName -> s0scalar,
	BlockName -> S0PARAMS,
	InteractionOrder -> {QS0, 1},
	Value -> 0.01,
	TeX -> Subscript[s0, scalar],
	Description -> "S0 scalar coupling"
  },

 WA0 == {
	ParameterType ->  External,
        ParameterName -> WA0,
	BlockName -> S0PARAMS,
	Value ->5,
	Description -> "A0 scalar coupling"
  }
};


 
(* ************************** *)
(* *****   Lagrangian   ***** *)
(* ************************** *)

LS0F =  I s0scalar SA0 tbar.Ga[5].t 
LS0ggfusionScalar =  -1/4 s0scalar SA0 FS[G,mu,nu,aa] FS[G,mu,nu,aa]
LS0 = LS0F + LS0ggfusionScalar
