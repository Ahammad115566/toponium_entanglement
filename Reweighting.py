'''
Python script to reweight  the LesHouches Event File according to https://arxiv.org/pdf/1007.0075v3.pdf

Run as: python3 Reweighting.py input.lhe output.lhe

A. Hammad- 09.01.2023
'''
import numpy as np
import os
import sys
import time
import shutil
path = os.getcwd()
##################################
############ colors ##############
def Red(prt): print("\033[91m {}\033[00m" .format(prt))
def Green(prt): print("\033[92m {}\033[00m" .format(prt))
def Yellow(prt): print("\033[93m {}\033[00m" .format(prt))
#############################################
if len(sys.argv) < 3:
    sys.exit(Red('Wrong inputs, please run as:  Rewight.py input.lhe output.lhe'))
arg_inp = str(sys.argv[-2])
arg_out = str(sys.argv[-1])
check_inp = arg_inp.rsplit('.')
check_out = arg_inp.rsplit('.')
if str(check_inp[-1]) != 'lhe':
    Yellow('Input file is detected with wrong extensions, "expected .lhe" .... We proceed!')
    time.sleep(6)
if str(check_out[-1]) != 'lhe':
    Yellow('Output file is detected with wrong extensions, "expected .lhe" .... We proceed!')
    time.sleep(6)    
################################################  
root=open (str(arg_inp))
fline=root.readline().rsplit()
if fline[-1] != 'version="3.0">':
    sys.exit(Red('Wrong Lehouches version please use version 3. exit'))
       
def get_block(inp,out,iD, blockname):
    block = []
    inblock = False
    blockname = blockname.lower()
    inFile = open(inp)
    f = open(out,'w+')
    q = 0
    for _, line in enumerate(inFile):
      if line.startswith(str(blockname)):
        q +=1
        inblock = False
        Green('Event Number  %i' %(q))
      if inblock:
       if line.lstrip().startswith(str(iD)+' '):
         y= line.rsplit()
         mt = y[-3]
         Et = y[-4]
         Et_new = float(Et)+ (float(Et)**2/(4*float(mt)))
         x_new = "{:.10e}".format(Et_new)
         line = line.replace(str(' '+y[-4]+' '),str(' '+x_new+' '))
       if line.lstrip().startswith(str(abs(iD))+' '):
         y= line.rsplit()
         mt = y[-3]
         Et = y[-4]
         Et_new = float(Et)+ (float(Et)**2/(4*float(mt)))
         x_new = "{:.10e}".format(Et_new)
         line = line.replace(str(' '+y[-4]+' '),str(' '+x_new+' '))  
      if line.startswith(str(blockname)):
        inblock = True  
      f.write(line)
    f.close()

get_block(str(arg_inp),str(arg_out),-6,'<event>')
Yellow('End of weighting!!')
Yellow('Output file: %s'%(str(path)+'/'+str(arg_out)))


